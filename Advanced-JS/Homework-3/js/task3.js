// Задание 3
// У нас есть объект user:

// Напишите деструктурирующее присваивание, которое:

// свойство name присвоит в переменную name
// свойство years присвоит в переменную age
// свойство isAdmin присвоит в переменную isAdmin (false, если нет такого свойства)

// Выведите переменные на экран.

const user1 = {
  name: "John",
  years: 30,
};

let parentElement = document.querySelector(".result"),
  { name: userName, years: age, isAdmin = false } = user1; 
  //не получается name присвоить в name, потому его можно использовать как есть или присвоить переменную с отличающимся именем, как сделал я!
parentElement.insertAdjacentHTML(
  "beforeend",
  `<p>name: ${userName}, age: ${age}, isAdmin: ${isAdmin}</p>`
);
