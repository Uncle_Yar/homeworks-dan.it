class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this.name;
  }
  set name(value) {
    this._name = value;
  }

  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }

  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary) {
    super(name, age, salary);

    this.lang = [];
  }

  get salary() {
    return this._salary * 3;
  }
}

const test_1 = new Programmer("Dima", 25, 400);
const test_2 = new Programmer("Yaroslav", 26, 500);
const test_3 = new Programmer("Andrey", 27, 600);

console.log(test_1);
console.log(test_2);
console.log(test_3.salary);
