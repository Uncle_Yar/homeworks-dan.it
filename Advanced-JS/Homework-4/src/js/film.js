class Film {
  constructor({ id, name, episodeId, openingCrawl, characters }) {
    this.id = id;
    this.title = name;
    this.episodeId = episodeId;
    this.openingСrawl = openingCrawl;
    this.characters = characters;

    if (characters) {
      this.getCharacters(characters);
    }
  }
  elements = {
    filmItem: document.createElement("div"),
    title: document.createElement("h2"),
    episodeId: document.createElement("h4"),
    openingСrawl: document.createElement("p"),
    charactersBlock: document.createElement("div"),
    characters: document.createElement("ul"),
  };

  render(parentElem) {
    const {
      filmItem,
      title,
      episodeId,
      openingСrawl,
      characters,
      charactersBlock,
    } = this.elements;
    filmItem.className = "film__item";
    title.innerText = this.title;
    title.className = "film__title";
    episodeId.innerText = `Episode# ${this.episodeId}`;
    episodeId.className = "film__episode";
    openingСrawl.innerHTML = this.openingСrawl;
    openingСrawl.className = "film__description-short";
    charactersBlock.className = "film__characters-block";
    characters.className = "film__characters-list";
    charactersBlock.append(characters);
    filmItem.prepend(title, episodeId, openingСrawl);
    parentElem.insertAdjacentElement("beforeend", filmItem);
  }

  getCharacters(charactersUrl) {
    if (charactersUrl.length > 0) {
      let { characters, charactersBlock, openingСrawl } = this.elements;
      let promises = charactersUrl.map((charUrl) => {
        return fetch(`${charUrl}`, {
          method: "get",
          headers: heads,
        }).then((resp) => resp.json());
      });

      Promise.all(promises).then((resp) => {
        if (resp.length > 0) {
          resp.forEach((ch) => {
            const char = new Character(ch);
            char.render(characters);
            charactersBlock.append(characters);
            openingСrawl.after(charactersBlock);
          });
        }
      });
    }
  }
}
