import React from 'react';
import {NavLink} from "react-router-dom";
import './Header.scss'

function Header() {
  return (
    <header className='head-bar'>
      <NavLink  exact to='/products'>Главная</NavLink>
      <NavLink exact to='/favorites'>Избранное</NavLink>
      <NavLink exact to='/cart'>Корзина</NavLink>
    </header>
  );
}

export default Header;