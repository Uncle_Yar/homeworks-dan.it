import React from "react";
import Button from "../Button/Button";
import Card from "../Card/Card";
import Modal from "../Modal/Modal";
import "./Cart.scss";

function Cart(props) {
  const actions = [
    <Button
      className="ok-button"
      key="okbtn"
      text="Ok"
      func={props.addToCart}
    />,
    <Button
      className="cancel-button"
      key="cncbtn"
      text="Cancel"
      func={props.showModal}
    />,
  ];
  const prodInCart = () => {
    let cartProds = JSON.parse(localStorage.getItem("cart"));
    let prodsArt = props.cards.map((el) => el.article);
    let matched = cartProds.filter((el) => prodsArt.indexOf(el) > -1);
    let product = props.cards.map(el =>matched.includes(el.article)?el:null);
    let filteredProduct = product.filter((el) => el);
    return filteredProduct;
  };
  const card = prodInCart();
  const prodCards = card.map((e) => (
    <Card
      id={e.article}
      key={e.article}
      card={e}
      fav={props.addToFav}
      cartAdd={props.showModal}
      favIcon={true}
      delIcon={true}
      isBtn={false}
    />
  ));

  return (
    <div className="buyer-cart">
      {props.cart.length !== 0 ? prodCards : <p>You cart are still empty...</p>}
      {props.isShowModal ? (
        <Modal
          func={props.showModal}
          header={"Are you sure you want to delete this item from your cart?"}
          text={
            <p className="modal-text">
              The product will be deleted from your cart.
            </p>
          }
          actions={actions}
        />
      ) : null}
    </div>
  );
}

export default Cart;
