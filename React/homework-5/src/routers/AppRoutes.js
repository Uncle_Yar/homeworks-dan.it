import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import Cart from "../pages/Cart/Cart";
import Favorites from "../pages/Favorites/Favorites";
import Products from "../pages/Products/Products";

function AppRoutes() {
  return (
    <Switch>
      <Redirect exact from="/" to="/products" />
      <Route exact path="/products">
        <Products />
      </Route>
      <Route exact path="/cart">
        <Cart />
      </Route>
      <Route exact path="/favorites">
        <Favorites />
      </Route>
    </Switch>
  );
}

export default AppRoutes;
