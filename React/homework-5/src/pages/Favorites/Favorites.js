import React from "react";
import Card from "../../components/Card/Card";
import Button from "../../components/Button/Button";
import Modal from "../../components/Modal/Modal";
import "./Favorites.scss";
import { connect, useDispatch, useSelector } from "react-redux";
import { favoritesSelectors, favoritesOperations } from "../../store/Favorites";
import { cartOperations } from "../../store/Cart";
import { productsSelectors } from "../../store/Products";

function Favorites(props) {
  const productList = useSelector(productsSelectors.getProducts());
  const productsOnFav = useSelector(favoritesSelectors.getFavorites());
  const dispatch = useDispatch();
  const showModal = (e) => {
    dispatch(
      favoritesOperations.setIsModal({
        bool: !props.isShowModal,
        target: e.target.id,
      })
    );
  };
  const addToCart = (e) => {
    productList.map((el) =>
      el.article === e.target.id
        ? dispatch(cartOperations.toggleCart(el))
        : null
    );
  };
  const deleteFromFav = (e) => {
    productsOnFav.map((el) =>
      el.article === e.target.parentElement.id
        ? dispatch(favoritesOperations.toggleFavorites(el))
        : null
    );
  };
  const actions = [
    <Button
      id={props.target}
      className="ok-button"
      key="okbtn"
      text="Ok"
      func={addToCart}
    />,
    <Button
      className="cancel-button"
      key="cncbtn"
      text="Cancel"
      func={showModal}
    />,
  ];

  const prodCards = productsOnFav.map((e) => (
    <Card
      key={e.article}
      card={e}
      fav={deleteFromFav}
      favIcon={true}
      func={showModal}
      isBtn={true}
    />
  ));

  return (
    <div className="buyer-favorites">
      {productsOnFav.length !== 0 ? (
        prodCards
      ) : (
        <p>Your favorites are still empty...</p>
      )}
      {props.isShowModal ? (
        <Modal
          target={props.target}
          func={showModal}
          header={"Are you sure you want to add this item to your cart?"}
          text={
            <p className="modal-text">
              The product will be added to your cart, then you can view a list
              of your purchases.
            </p>
          }
          actions={actions}
        />
      ) : null}
    </div>
  );
}

const mapStateToProps = (state) => {
  return {
    isShowModal: state.favorites.isShowModal,
    target: state.favorites.target,
  };
};
export default connect(mapStateToProps, null)(Favorites);
