import React from "react";
import Button from "../../components/Button/Button";
import Card from "../../components/Card/Card";
import Modal from "../../components/Modal/Modal";
import CartForm from "../../components/CartForm/CartForm";
import "./Cart.scss";
import { connect, useDispatch, useSelector } from "react-redux";
import { cartSelectors, cartOperations } from "../../store/Cart";
import { favoritesOperations } from "../../store/Favorites";
import { productsSelectors } from "../../store/Products";

function Cart(props) {
  const productsOnCart = useSelector(cartSelectors.getCart());
  const productList = useSelector(productsSelectors.getProducts());
  const dispatch = useDispatch();
  const showModal = (e) => {
    dispatch(
      cartOperations.setIsModal({
        bool: !props.isShowModal,
        target: e.target.id,
      })
    );
  };
  const deleteFromCart = () => {
    productsOnCart.map((el) =>
      el.article === props.target
        ? dispatch(cartOperations.toggleCart(el))
        : null
    );
  };
  const addToFav = (e) => {
    productList.map((el) =>
      el.article === e.target.parentElement.id
        ? dispatch(favoritesOperations.toggleFavorites(el))
        : null
    );
  };

  const sumTotals = () => {
    let getPrice = productsOnCart.reduce((acc, curr) => {
      let cur = curr.price;
      return acc + cur;
    }, 0);
    return (
      <div>
        <p>Total items: {productsOnCart.length}</p>
        <h3>Total: {getPrice} $</h3>
      </div>
    );
  };
  const actions = [
    <Button
      id={props.target}
      className="ok-button"
      key="okbtn"
      text="Ok"
      func={deleteFromCart}
    />,
    <Button
      className="cancel-button"
      key="cncbtn"
      text="Cancel"
      func={showModal}
    />,
  ];

  const prodCards = productsOnCart.map((e) => (
    <Card
      id={e.article}
      key={e.article}
      card={e}
      fav={addToFav}
      cartAdd={showModal}
      favIcon={true}
      delIcon={true}
      isBtn={false}
    />
  ));

  return (
    <div className="buyer-cart">
      {productsOnCart.length !== 0 ? (
        <div className="cart-items">{prodCards}</div>
      ) : (
        <p>You cart are still empty...</p>
      )}
      {productsOnCart.length !== 0 ? (
        <div className="cart-info">
          <div>
            <h3>Cart info</h3>
            {sumTotals()}
          </div>
          <CartForm />
        </div>
      ) : null}
      {props.isShowModal ? (
        <Modal
          func={showModal}
          header={"Are you sure you want to delete this item from your cart?"}
          text={
            <p className="modal-text">
              The product will be deleted from your cart.
            </p>
          }
          actions={actions}
        />
      ) : null}
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    isShowModal: state.cart.isShowModal,
    target: state.cart.target,
    order: state.cart.order,
  };
};
export default connect(mapStateToProps, null)(Cart);
