import { TOGGLE_CART, TOGGLE_MODAL, CART_ORDER } from "./types";

export const toggleCart = (productObject) => ({
  type: TOGGLE_CART,
  payload: productObject,
});
export const cartOrder = (data) => ({
  type: CART_ORDER,
  payload: data,
});

export const setIsModal = (data) => ({
  type: TOGGLE_MODAL,
  payload: data,
});
