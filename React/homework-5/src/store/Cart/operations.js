import { toggleCart, setIsModal, cartOrder } from "./actions";

const actions = { toggleCart, setIsModal, cartOrder };

export default actions;
