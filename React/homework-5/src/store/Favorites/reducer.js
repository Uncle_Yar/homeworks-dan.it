import { TOGGLE_FAVORITES, TOGGLE_MODAL } from "./types";

const initialState = {
  data: JSON.parse(localStorage.getItem("favorites")) || [],
  isShowModal: false,
  target: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_FAVORITES: {
      const productInfo = action.payload;
      if (state.data.find((itm) => itm.article === productInfo.article)) {
        const products = state.data.filter(
          (product) => product.article !== productInfo.article
        );
        localStorage.setItem("favorites", JSON.stringify([...products]));
        return { ...state, data: products };
      } else {
        const products = [...state.data, productInfo];
        localStorage.setItem("favorites", JSON.stringify(products));
        return { ...state, data: products };
      }
    }
    case TOGGLE_MODAL: {
      return {
        ...state,
        isShowModal: action.payload.bool,
        target: action.payload.target,
      };
    }

    default:
      return state;
  }
};

export default reducer;
