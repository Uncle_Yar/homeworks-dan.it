import { combineReducers } from "redux";
import products from "./Products";
import cart from "./Cart";
import favorites from "./Favorites";

const reducers = combineReducers({
  products,
  cart,
  favorites,
});

export default reducers;
