import { SET_PRODUCTS, TOGGLE_MODAL } from "./types";

export const setProducts = (products) => ({
  type: SET_PRODUCTS,
  payload: products,
});

export const setIsModal = (data) => ({
  type: TOGGLE_MODAL,
  payload: data,
});
