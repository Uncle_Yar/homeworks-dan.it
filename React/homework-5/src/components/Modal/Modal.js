import React from "react";
import "./Modal.scss";

const Modal = ({ target, header, text, func, actions }) => {
  return (
    <div id={target} className="modal-wrapper" onClick={func}>
      <div className="modal">
        <div className="modal-header">
          <h1 className="modal-title">{header}</h1>
        </div>
        {text}
        <div>{actions}</div>
      </div>
    </div>
  );
};

export default Modal;
