import React from "react";
import "./Input.scss";

const Input = (props) => {
  const { field, form, ...rest } = props;
  const { name } = field;

  return (
    <div>
      <input className="appInput" {...rest} {...field} />
      {form.touched[name] && form.errors[name] && (
        <div className="error">{form.errors[name]}</div>
      )}
    </div>
  );
};
export default Input;
