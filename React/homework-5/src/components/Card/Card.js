import React from "react";
import "./Card.scss";
import Button from "../Button/Button";
import FavIcon from "../Icon/FavIcon";
import DelIcon from "../Icon/DelIcon";
import { useSelector } from "react-redux";
import { favoritesSelectors } from "../../store/Favorites";

const Card = (props) => {
  const isInFavorites = useSelector(
    favoritesSelectors.isProductInFavorites(props.card.article)
  );

  return (
    <div id={props.id} className="product-card">
      {props.delIcon ? (
        <DelIcon
          id={props.card.article}
          className="del-icon"
          func={props.cartAdd}
        />
      ) : null}

      <h3>{props.card.title}</h3>
      <img
        src={props.card.imgUrl}
        style={{ width: 100 }}
        alt={props.card.title}
      ></img>
      <p className="product-article">Article: {props.card.article}</p>
      <p>Price: {props.card.price}$</p>
      <p>Color: {props.card.color}</p>
      <div>
        {props.isBtn ? (
          <Button
            id={props.card.article}
            text="Add to cart"
            className="card-button"
            func={props.func}
          />
        ) : null}
        {props.favIcon ? (
          <FavIcon
            id={props.card.article}
            className="favor-icon"
            fill={isInFavorites}
            func={props.fav}
          />
        ) : null}
      </div>
    </div>
  );
};

export default Card;
