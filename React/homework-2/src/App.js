import React, { Component } from "react";
import axios from "axios";
import "./App.css";
import Products from "./components/Products/Products";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";

class App extends Component {
  state = {
    cards: [],
    showModal: false,
  };
  targetProd = null;

  addToCart = () => {
    let cartProds = JSON.parse(localStorage.getItem("cart"));
    let newProds = cartProds.map((el) => el);
    newProds.push(this.targetProd);
    localStorage.setItem("cart", JSON.stringify(newProds));
    this.setState({
      ...this.state,
      showModal: false,
    });
  };
  addToFav = (e) => {
    let favProds = JSON.parse(localStorage.getItem("favorites"));
    let newProds = favProds.map((el) => el);
    if (!newProds.includes(e.target.parentElement.id)) {
      newProds.push(e.target.parentElement.id);
      localStorage.setItem("favorites", JSON.stringify(newProds));
      e.target.classList.toggle("active");
    } else if (newProds.includes(e.target.parentElement.id)) {
      let index = newProds.indexOf(e.target.parentElement.id);
      newProds.splice(index, 1);
      localStorage.setItem("favorites", JSON.stringify(newProds));
      e.target.classList.toggle("active");
    }
  };
  actions = [
    <Button
      className="ok-button"
      key="okbtn"
      text="Ok"
      func={this.addToCart}
    />,
    <Button
      className="cancel-button"
      key="cncbtn"
      text="Cancel"
      func={() =>
        this.setState({
          ...this.state,
          showModal: false,
        })
      }
    />,
  ];
  componentDidMount() {
    axios("/products.json").then((res) => {
      this.setState({ cards: res.data });
    });
    if (!localStorage.getItem("cart")) localStorage.setItem("cart", "[]");
    if (!localStorage.getItem("favorites"))
      localStorage.setItem("favorites", "[]");
  }
  showModal = (ev) => {
    const check = !this.state.showModal;
    this.setState({
      ...this.state,
      showModal: check,
    });
    this.targetProd = ev.target.id;
  };

  render() {
    const { cards, showModal } = this.state;
    const actions = this.actions;
    return (
      <div className="App">
        <Products products={cards} func={this.showModal} fav={this.addToFav} />
        {showModal ? (
          <Modal
            func={this.showModal}
            header={"Are you sure you want to add this item to your cart?"}
            text={
              <p className="modal-text">
                The product will be added to your cart, then you can view a list
                of your purchases.
              </p>
            }
            actions={actions}
          />
        ) : null}
      </div>
    );
  }
}

export default App;
