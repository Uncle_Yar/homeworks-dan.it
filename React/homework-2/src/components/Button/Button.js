import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Button.scss";

class Button extends Component {
  render() {
    const { id, className, text, func } = this.props;
    return (
      <button id={id} className={className} onClick={func}>
        {text}
      </button>
    );
  }
}

Button.propTypes = {
  id: PropTypes.string,
  className: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  func: PropTypes.func,
};

Button.defaultProps = {
  id: "noneID",
  func: null,
};
export default Button;
