import React, { useEffect } from "react";
import Button from "../../components/Button/Button";
import Card from "../../components/Card/Card";
import Modal from "../../components/Modal/Modal";
import { connect, useDispatch, useSelector } from "react-redux";
import { productsOperations, productsSelectors } from "../../store/Products";
import { cartOperations } from "../../store/Cart";
import { favoritesOperations } from "../../store/Favorites";
import "./Products.scss";

const Products = (props) => {
  const productList = useSelector(productsSelectors.getProducts());
  const dispatch = useDispatch();
  useEffect(() => dispatch(productsOperations.fetchProducts()), [dispatch]);

  const showModal = (e) => {
    dispatch(
      productsOperations.setIsModal({
        bool: !props.isShowModal,
        target: e.target.id,
      })
    );
  };
  const addToFav = (e) => {
    productList.map((el) =>
      el.article === e.target.parentElement.id
        ? dispatch(favoritesOperations.toggleFavorites(el))
        : null
    );
  };
  const addToCart = () => {
    productList.map((el) =>
      el.article === props.target
        ? dispatch(cartOperations.toggleCart(el))
        : null
    );
  };

  const actions = [
    <Button
      id={props.target}
      className="ok-button"
      key="okbtn"
      text="Ok"
      func={addToCart}
    />,
    <Button
      className="cancel-button"
      key="cncbtn"
      text="Cancel"
      func={showModal}
    />,
  ];

  const prodCards = productList.map((e) => (
    <Card
      id={e.id}
      key={e.article}
      card={e}
      fav={addToFav}
      func={showModal}
      favIcon={true}
      isBtn={true}
    />
  ));

  return (
    <div className="products-wrapper">
      {prodCards}
      {props.isShowModal ? (
        <Modal
          target={props.target}
          func={showModal}
          header={"Are you sure you want to add this item to your cart?"}
          text={
            <p className="modal-text">
              The product will be added to your cart, then you can view a list
              of your purchases.
            </p>
          }
          actions={actions}
        />
      ) : null}
    </div>
  );
};
const mapStateToProps = (state) => {
  return {
    isShowModal: state.products.isShowModal,
    target: state.products.target,
  };
};

export default connect(mapStateToProps, null)(Products);
