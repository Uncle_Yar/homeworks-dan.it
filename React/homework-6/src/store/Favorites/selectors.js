const getFavorites = () => (state) => state.favorites.data;

const isProductInFavorites = (id) => (state) => {
  return state.favorites.data.some(function (el) {
    return el.article === id;
  });
};
const selectors = {
  isProductInFavorites,
  getFavorites,
};
export default selectors;
