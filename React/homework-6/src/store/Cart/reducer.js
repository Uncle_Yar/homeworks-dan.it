import { TOGGLE_CART, TOGGLE_MODAL, CART_ORDER } from "./types";

const initialState = {
  data: JSON.parse(localStorage.getItem("cart")) || [],
  order: [],
  isShowModal: false,
  target: null,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case TOGGLE_CART: {
      const productInfo = action.payload;
      if (state.data.find((itm) => itm.article === productInfo.article)) {
        const products = state.data.filter(
          (product) => product.article !== productInfo.article
        );
        localStorage.setItem("cart", JSON.stringify([...products]));
        return { ...state, data: products };
      } else {
        const products = [...state.data, productInfo];
        localStorage.setItem("cart", JSON.stringify(products));
        return { ...state, data: products };
      }
    }
    case TOGGLE_MODAL: {
      return {
        ...state,
        isShowModal: action.payload.bool,
        target: action.payload.target,
      };
    }
    case CART_ORDER: {
      localStorage.setItem("cart", JSON.stringify([]));
      let order = state.data
        .map((el) => {
          return {
            description: el.title,
            article: el.article,
            price: el.price,
          };
        })
        .concat(action.payload);
      order.unshift(...order.splice(state.data.length, 1));
      console.log("ORDER", order);
      return {
        ...state,
        data: [],
      };
    }

    default:
      return state;
  }
};

export default reducer;
