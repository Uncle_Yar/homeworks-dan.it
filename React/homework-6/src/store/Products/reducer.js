import { SET_PRODUCTS, TOGGLE_MODAL } from "./types";

const initialState = {
  data: [],
  isShowModal: false,
  target: null,
  favorites: JSON.parse(localStorage.getItem("favorites")) || [],
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PRODUCTS: {
      return { ...state, data: action.payload };
    }
    case TOGGLE_MODAL: {
      return {
        ...state,
        isShowModal: action.payload.bool,
        target: action.payload.target,
      };
    }
    default:
      return state;
  }
};

export default reducer;
