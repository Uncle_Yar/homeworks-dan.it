import React from "react";
import { Formik, Form, Field } from "formik";
import Input from "../Input/Input";
import * as yup from "yup";
import { cartOperations } from "../../store/Cart";
import { useDispatch } from "react-redux";

const isRequired = "This field is required";
const schema = yup.object().shape({
  name: yup.string().required(isRequired).min(2, "Enter min 2 characters"),
  secondName: yup
    .string()
    .required(isRequired)
    .min(2, "Enter min 2 characters"),
  age: yup
    .number()
    .required(isRequired)
    .moreThan(17, "You must be min 18 years old")
    .typeError("Age must be a number"),
  address: yup.string().required(isRequired).min(2, "Enter min 2 characters"),
  mobNumber: yup
    .number()
    .required(isRequired)
    .min(12, "Enter min 12 characters of number"),
});
export const CartForm = ({ __testFunc__ }) => {
  const dispatch = useDispatch();
  const handleSubmit = (values) => {
    dispatch(cartOperations.cartOrder(values));
  };

  return (
    <Formik
      initialValues={{
        name: "",
        secondName: "",
        age: "",
        address: "",
        mobNumber: "",
      }}
      onSubmit={__testFunc__ || handleSubmit}
      validationSchema={schema}
    >
      {(formikProps) => {
        return (
          <Form>
            <h3>Create order</h3>
            <div>
              <Field
                component={Input}
                name="name"
                type="text"
                placeholder="Name"
              />
            </div>
            <div>
              <Field
                component={Input}
                name="secondName"
                type="text"
                placeholder="Second name"
              />
            </div>
            <div>
              <Field
                component={Input}
                name="age"
                type="text"
                placeholder="Age"
              />
            </div>
            <div>
              <Field
                component={Input}
                name="address"
                type="text"
                placeholder="Delivery address"
              />
            </div>
            <div>
              <Field
                component={Input}
                name="mobNumber"
                type="text"
                placeholder="Mobile number starts (380**)"
              />
            </div>
            <div>
              <button
                data-testid="sbmBtn"
                className="order-submit"
                type="submit"
              >
                Checkout
              </button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export default CartForm;
