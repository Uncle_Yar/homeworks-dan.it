import Favicon from "./FavIcon";
import { render } from "@testing-library/react";
import userEvent from "@testing-library/user-event";

const props = {
  func: jest.fn(),
  className: "test-fav-icon",
};

describe("Testing Favicon component", () => {
  test("Testing Favicon props", () => {
    const { container } = render(<Favicon {...props} />);
    const button = container.querySelector(`.${props.className}`);
  });

  test("Testing Favicon get className", () => {
    const { container } = render(<Favicon {...props} />);
    expect(container.getElementsByClassName(props.className).length > 0).toBe(
      true
    );
  });
  test("Testing Favicon function work", () => {
    const { container } = render(<Favicon {...props} />);
    const button = container.querySelector(`.${props.className}`);

    expect(props.func).not.toHaveBeenCalled();
    userEvent.click(button);
    expect(props.func.mock.calls.length).toBe(1);
  });
});
