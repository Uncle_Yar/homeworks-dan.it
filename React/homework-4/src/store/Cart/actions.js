import {TOGGLE_CART,TOGGLE_MODAL}from "./types";

 export const toggleCart = (productObject) => ({
  type: TOGGLE_CART,
  payload: productObject,
});

export const setIsModal = (data) => ({
  type: TOGGLE_MODAL,
  payload: data,
});
