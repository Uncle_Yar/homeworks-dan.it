const getCart = () => (state) => state.cart.data


const isProductInCart = (article) => (state) =>
    !!state.cart.data.find((product) => product.article === article)
    const selectors={
    getCart,
    isProductInCart,
}
export default selectors     
