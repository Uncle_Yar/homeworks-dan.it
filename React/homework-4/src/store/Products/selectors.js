const getProducts = () => (state) => state.products.data;

 const selectors={
  getProducts,
};
export default selectors