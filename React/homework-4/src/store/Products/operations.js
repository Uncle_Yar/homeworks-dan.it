import { setProducts, setIsModal } from "./actions";
import axios from "axios";

const fetchProducts = () => (dispatch) => {
  axios("/products.json").then((res) => {
    dispatch(setProducts(res.data));
  });
};

const actions = { setIsModal, fetchProducts };
export default actions;
