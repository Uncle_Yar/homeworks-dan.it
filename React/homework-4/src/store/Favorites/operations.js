import {toggleFavorites,setIsModal} from './actions'

const actions = {toggleFavorites,setIsModal}

export default actions
