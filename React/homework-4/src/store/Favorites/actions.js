import {TOGGLE_FAVORITES,TOGGLE_MODAL} from "./types";

export const toggleFavorites = (article) =>
({
    type: TOGGLE_FAVORITES,
    payload: article
})
export const setIsModal = (data) => ({
  type: TOGGLE_MODAL,
  payload: data,
});


