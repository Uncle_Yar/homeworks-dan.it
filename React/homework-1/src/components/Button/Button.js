import React, { Component } from 'react';
import './Button.scss'

class Button extends Component {
  render() {
    const{id, className,text, backgroundColor,func}=this.props
    return (
      <button id={id} className={className} style={{backgroundColor}} onClick={func}>{text}</button>
    );
  }
}

export default Button;