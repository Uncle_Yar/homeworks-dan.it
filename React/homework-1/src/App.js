import React, { Component } from "react";
import "./App.css";
import Button from "./components/Button/Button.js";
import Modal from "./components/Modal/Modal.js";

class App extends Component {
  state = {
    causedModal: null,
  };

  actions = [
    <Button
      className="main-button"
      backgroundColor="rgb(166, 65, 51)"
      key="okbtn"
      text="Ok"
      func={() => alert("WELL OK!")}
    />,
    <Button
      className="main-button"
      backgroundColor="rgb(166, 65, 51)"
      key="cnsbtn"
      func={() =>
        this.setState(
          (state) =>
            (state = {
              ...state,
              causedModal: null,
            })
        )
      }
      text="Cancel"
      color="black"
    />,
  ];

  render() {
    const { causedModal } = this.state;
    const actions = this.actions;
    return (
      <div className="App">
        <div style={{ margin: 100 + "px" }}>
          <Button
            id="opnfirstmod"
            className="main-button"
            func={this.handleClick}
            backgroundColor="red"
            text="Open first modal"
          />
          <Button
            id="opnsecmod"
            className="main-button"
            func={this.handleClick}
            backgroundColor="blue"
            text="Open second modal"
          />
        </div>
        {causedModal==="opnfirstmod"?<Modal
                backgroundColor="rgb(232, 65, 51)"
                closeButton={true}
                func={this.closeBtn}
                header={"Do you want to delete this file?"}
                text={
                  <p className="modal-text">
                    Once you delete this file, it won't be possible to undo this
                    action. Are you sure you want to delete it?
                  </p>
                }
                actions={actions}
              />:null}
              {causedModal==='opnsecmod'?<Modal
                backgroundColor="rgb(0, 169, 254)"
                closeButton={false}
                func={this.closeBtn}
                header={"Lorem ipsum ! (magic words))"}
                text={
                  <p className="modal-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. At
                    deserunt dignissimos, iure nemo nisi perferendis porro quo
                    veniam. Amet asperiores at, dolor enim officia quae quisquam
                    ratione rem temporibus velit.
                  </p>
                }
                actions={actions}
              />:null}
      </div>
    );
  }

  closeBtn = () => {
    this.setState(
      (state) =>
        (state = {
          ...state,
          causedModal: null,
        })
    );
  };

  handleClick = (e) => {
    return this.setState(
      (state) =>
        (state = {
          ...state,
          causedModal: e.target.id
        })
    );
  };
}

export default App;
