function createNewUser(name, surname, birthday) {
    let newUser = {
        firstName: name,
        lastName: surname,
        birthdayDate: birthday,
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase()
        },
        getAge() {
            let ageArray = this.birthdayDate.split('.')
            let newAgeArray = new Date(Number(ageArray[2]), Number(ageArray[1]), Number(ageArray[0]))
            let todayDate = new Date()
            let todayDateNew = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate())
            var nowYearDay = new Date(todayDateNew.getFullYear(), newAgeArray.getMonth(), newAgeArray.getDate())
            let age

            age = todayDateNew.getFullYear() - newAgeArray.getFullYear()
            if (todayDateNew < nowYearDay) {
                age = age - 1;
            }
            return age

        },
        getPassword() {
            let passYear = this.birthdayDate.split('.')
            let pass = `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${passYear[2]}`
            return pass
        }
    }
    return newUser
}

let information = createNewUser(prompt('Enter your name.'), prompt('Enter your surname.'), prompt('Enter your birthday dd.mm.yyyy'))

console.log(information)
console.log(information.getAge())
console.log(information.getLogin())
console.log(information.getPassword())