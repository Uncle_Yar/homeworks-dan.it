$(document).ready(function() {
  $("a.scrollto").click(function() {
    let elementClick = $(this).attr("href")
    let destination = $(elementClick).offset().top;
    jQuery("html:not(:animated),body:not(:animated)").animate({
      scrollTop: destination
    }, 1500);
    return false;
  });
});

let btn = $('#button');

$(window).scroll(function() {
  if ($(window).scrollTop() > $(window).height()) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, 1500);
});



$('.news').click(function(){
  $('.news-cards').slideToggle('slow')  
})
