
let slides = document.querySelectorAll('.image-to-show')
let currentSlide = 0
let slideInterval = setInterval(nextSlide,3000)
slides[currentSlide].className = 'image-to-show-active'

let parrentEl = document.querySelector('.images-wrapper')
if(document.querySelector('.play-btns') === null){
   parrentEl.insertAdjacentHTML('beforeend','<div class = "play-btns"><button class="stop btn-shadow" style="cursor:pointer">Stop sliding</button><button class = "continue btn-shadow" style="cursor:pointer">Continue</button></div>')        
}
document.querySelector('.stop').addEventListener('click', ()=>{
    clearInterval(slideInterval)
})

document.querySelector('.continue').addEventListener('click', ()=>{        
    if(slideInterval){
        clearInterval(slideInterval)
    }slideInterval = setInterval(nextSlide,3000);
})

function nextSlide() {
    if(slides[currentSlide].classList.contains('image-to-show')){
        slides[currentSlide].className = 'image-to-show-active'
    }else if(slides[currentSlide].classList.contains('image-to-show-active')){
    slides[currentSlide].className = 'image-to-show'    
    currentSlide = ++currentSlide%slides.length;    
    slides[currentSlide].className = 'image-to-show-active'
    }      
}


