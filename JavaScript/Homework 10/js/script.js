let inputForm = document.querySelector('.password-form')
if (inputForm) {
    inputForm.addEventListener('click', function (event) {

        if (event.target.classList.contains('icon-password')) {
            if (event.target.classList.contains('fa-eye')) {
                event.target.className = event.target.className.replace('fa-eye', 'fa-eye-slash')
                event.target.parentElement.querySelector('input').type = 'text'
            } else {
                event.target.className = event.target.className.replace('fa-eye-slash', 'fa-eye')
                event.target.parentElement.querySelector('input').type = 'password'
            }
        }

    })
    let submit = document.querySelector('.submit-btn')
    submit.addEventListener('click', (event) => {
        event.preventDefault()
        let inputs = document.querySelectorAll('input')
        inputs = [...inputs]
        if (inputs.reduce((first, second) => first.value === second.value) && inputs.every(elem => elem.value !== '')) {
            if (document.querySelector('.warning') !== null) {
                document.querySelector('.warning').remove('warning')
                alert('You are welcome')
            } else alert('You are welcome')
        } else if (inputs.every(elem => elem.value === '' && document.querySelector('.warning') === null)) {
            let warning = document.createElement('p')
            warning.innerText = 'Поля не могут быть пустыми!'
            warning.style.color = 'red'
            warning.classList.add('warning')
            submit.after(warning)
        } else {
            if (document.querySelector('.warning') === null) {
                let warning = document.createElement('p')
                warning.innerText = 'Нужно ввести одинаковые значения!'
                warning.style.color = 'red'
                warning.classList.add('warning')
                submit.after(warning)
            }
            return
        }
    })
}